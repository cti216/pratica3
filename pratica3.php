<?php
require_once "autoloader.php";
$objProdutos = new cti216\Model\Produto;

 if (isset($_REQUEST["acao"]) && !empty($_REQUEST["acao"])) { //Checa se existe a informação 'acao'
     $acao = $_REQUEST["acao"];
     switch ($acao) { //Verifica o conteudo da variavel '$acao'
         case "listaCategorias": {
             $objProdutos->listaCategorias();
             break;
         }
         case "listaProdutos": {
             if (isset($_REQUEST["idCategoria"]) && !empty($_REQUEST["idCategoria"])) {
                 $idCategoria = $_REQUEST["idCategoria"];
                 $objProdutos->listaProdutos($idCategoria);
             }
             break;
         }
         case "descricao": {
             if (isset($_REQUEST["idProdutos"]) && !empty($_REQUEST["idProdutos"])) {
                 $idProdutos = $_REQUEST['idProdutos'];
                 $objProdutos->buscaProdutos($idProdutos);
             }
             break;
         }
     }
 }
/**
 *
 */

?>