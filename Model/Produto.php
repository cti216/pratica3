<?php
namespace cti216\Model;

class Produto{

function listaCategorias(){

    require_once "conexao.php";
    $categorias = array();

    $categorias[0]="Selecione a Categoria";
    $sql="Select * from tbl_categoria;";
    $rs = mysqli_query($con,$sql);
    while($reg = mysqli_fetch_assoc($rs)){
        $categorias[$reg['id']]=$reg['nome'];
    }

	$resposta["json"]= $categorias;
	echo json_encode($resposta);
}

function listaProdutos($idCategorias){
// se quiser simular um processamento demorado,
// pode descomentar o laco for( )
//	for($i=0;$i<20000000;$i++);
    require_once "conexao.php";
    $produtos=array();

	$sql="Select * from tbl_produto WHERE idCategoria = $idCategorias;";
    $rs = mysqli_query($con,$sql);

    $produtos[0]="Selecione o Produto";
    while($reg = mysqli_fetch_assoc($rs)){
        $produtos[$reg['id']]=$reg['nome'];
    }

	$resposta["json"]=$produtos;
	echo json_encode($resposta);
}

function buscaProdutos($idProdutos){

    require_once "conexao.php";
    $produto = array();

    $sql="Select * from tbl_produto WHERE id=$idProdutos;";
    $res= mysqli_query($con,$sql);

    while($reg = mysqli_fetch_assoc($res)){
        $produto[0] = $reg['nome'];
        $produto[1] = $reg['preco'];
        $produto[2]= $reg['estoque'];
    }

    $resposta["json"]=$produto;
    echo json_encode($resposta);
}
}